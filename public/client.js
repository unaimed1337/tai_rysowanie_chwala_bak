var socket = io.connect();
var color = "black"

document.addEventListener("DOMContentLoaded",function(){

    var mouse ={
        click:false,
        move:false,
        pos:{x:0,y:0},
        pos_prev:false
    };
    var canvas=document.getElementById('rysunek');
    var context = canvas.getContext('2d');
    var height = window.innerHeight;
    var width  = window.innerWidth;
    canvas.width= width;
    canvas.height= height;

    canvas.onmousedown=function(e)
    {
        mouse.click = true;
    };
    canvas.onmouseup=function(e)
    {
        mouse.click=false;
    };
    canvas.onmousemove=function(e)
    {
        mouse.pos.x = e.clientX / width;
        mouse.pos.y = e.clientY / height;
        mouse.move = true;
    };
    socket.on('clearView', function(){
        context.clearRect(0, 0, width, height);
    });
    console.log('rysuje');
    socket.on('draw', function (data) {
        var line = data.line;
        var color = data.color;
        context.strokeStyle = color;
        context.beginPath();
        context.moveTo(line[0].x * width, line[0].y * height);
        context.lineTo(line[1].x * width, line[1].y * height);
        context.stroke();
    });

    socket.on('saveCanvas',function(){
        console.log("saveCanvas");
        var link = document.createElement('a');
        link.download="image.png";
        link.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        link.click();
    });

    function main()
    {
        if (mouse.click && mouse.move && mouse.pos_prev) {
            socket.emit('draw', { line: [ mouse.pos, mouse.pos_prev ], color: color });
            mouse.move = false;
        }
        mouse.pos_prev = {x: mouse.pos.x, y: mouse.pos.y};
        setTimeout(main, 25);
    }
    main(); // zapętla
});
function saveCanvas(){
    socket.emit('saveCanvas',true);
}
function clearView(){
    socket.emit('clearView', true);
}

function setColorBlack() {
    color = "black";
}

function setColorBlue() {
    color = "blue";
}

function setColorGreen() {
    color = "green";
}

function setColorRed() {
    color = "red";
}