var express = require('express'),
    app=express(),
    http= require('http'),
    io=require('socket.io');

var server = http.createServer(app);
var io=require('socket.io').listen(server);
server.listen(1234);

app.use(express.static(__dirname));
console.log("Server running on http://localhost:1234");

var history =[];
io.on('connection',function(socket) // jak ktos się podłączy
{
    for (var i in history)
    {
        socket.emit('draw', history[i]);
    }

    socket.on('clearView', function(){
       history = [];
        io.emit('clearView', true);
    });
    socket.on('saveCanvas',function(){
        io.emit('saveCanvas',true);

    });

    socket.on('draw', function(data)
    {
       // history.pushback(data.line);
        history.push(data);
        io.emit('draw',{line: data.line, color: data.color}); // wysłać wszystkim podłączonym
    });
});
